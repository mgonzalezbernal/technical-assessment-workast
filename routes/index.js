
const express = require('express');

const router = express.Router();

const isAuth = require('../middlewares/auth');
const articles = require('../controllers/articles');
const users = require('../controllers/users');
const healthCheck = require('../controllers/healthCheck');


router.get('/articles', isAuth, articles.getArticles);
router.get('/article/:id', isAuth, articles.getArtilceById);
router.get('/article-by-user-id/:id', isAuth, articles.getArtilcesByUserId);
router.get('/users', isAuth, users.getUsers);
router.get('/user/:id', isAuth, users.getUserById);
router.get('/health', healthCheck.health);
router.get('/alive', healthCheck.isAlive);


router.put('/article/:id', isAuth, articles.editArticle);
router.put('/user/:id', isAuth, users.editUser);

router.post('/new-article', isAuth, articles.newArticle);
router.post('/new-user', isAuth, users.newUser);
router.post('/articles-by-tags/', isAuth, articles.getArtilcesByTags);

router.delete('/article/:id', isAuth, articles.deleteArticle);
router.delete('/user/:id', isAuth, users.deleteUser);

module.exports = router;
