
const UserModel = require('../models/User');

async function getUsers(_req, res) {
  try {
    const users = await UserModel.find();
    return res.status(200).send(users);
  } catch (error) {
    return res.status(500).send(error);
  }
}

async function editUser(req, res) {
  try {
    const { id } = req.params;
    const { name, avatar } = req.body;
    const user = await UserModel.findOne({ _id: id });
    if (user) {
      user.name = name;
      user.avatar = avatar;
      await user.save((err) => {
        if (err) return res.status(500).send({ message: `Error when try to edit User:${err}` });
        try {
          return res.status(200).send({ message: 'User updated ', user });
        } catch (error) {
          return error;
        }
      });
    } else {
      res.status(404).send({ message: 'The user id not exist' });
    }
  } catch (error) {
    return res.status(500).send((error));
  }
}

async function newUser(req, res) {
  try {
    const user = new UserModel(req.body);
    await user.save((err) => {
      if (err) return res.status(500).send({ message: `Error when create a new User:${err}` });
      try {
        return res.status(201).send({ user });
      } catch (error) {
        return error;
      }
    });
  } catch (error) {
    return res.status(500).send((error));
  }
}

async function getUserById(req, res) {
  try {
    const { id } = req.params;
    const user = await UserModel.findOne({ _id: id });
    if (user) {
      return res.status(200).send((user));
    }
    return res.status(404).send({ message: 'The user id not exist' });
  } catch (error) {
    return res.status(500).send((error));
  }
}

async function deleteUser(req, res) {
  try {
    const { id } = req.params;
    await UserModel.remove({ _id: id });
    return res.status(200).send({ message: 'User deleted ' });
  } catch (error) {
    return res.status(404).send((error));
  }
}

module.exports = {
  getUsers,
  getUserById,
  editUser,
  newUser,
  deleteUser,
};
