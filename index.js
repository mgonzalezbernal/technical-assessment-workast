const server = require('./setup/server');
const db = require('./setup/db');

db.connect()
  .then(() => {
    server.listen(server.get('port'), () => {
      console.log(`server on port ${server.get('port')}`);
    });
  });
