const isValidToken = require('../services/servicesToken');

function auth(req, res, next) {
  if (!req.headers.authorization) {
    throw res.send({ message: 'Unauthorized' });
  }
  const token = req.headers.authorization;
  isValidToken(token)
    .then(() => {
      next();
    })
    .catch((err) => {
      res.send(err);
    });
}

module.exports = auth;
