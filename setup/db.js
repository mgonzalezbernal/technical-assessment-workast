require('dotenv').config();
const mongoose = require('mongoose');
// eslint-disable-next-line import/no-extraneous-dependencies
const { MongoMemoryServer } = require('mongodb-memory-server');

const {
  DB_USER,
  DB_PASS,
  DB_HOST,
  DB_PORT,
  DB_NAME,
  NODE_ENV,
} = process.env;

     

function connect() {
  return new Promise((resolve, reject) => {
    if (NODE_ENV === 'test') {
      const mongoServer = new MongoMemoryServer();
      mongoose.Promise = Promise;
      mongoServer.getUri().then((mongoUri) => {
        const mongoUriw = `mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`;

        const mongooseOpts = {

          useNewUrlParser: true,
          useUnifiedTopology: true,
          useCreateIndex: true,
        };

        mongoose.connect(mongoUriw, mongooseOpts);

        mongoose.connection.on('error', (e) => {
          if (e.message.code === 'ETIMEDOUT') {
            console.log(e);
            mongoose.connect(mongoUriw, mongooseOpts);
          }
          console.log(e);
        });

        mongoose.connection.once('open', () => {
          console.log(`MongoDB successfully connected to ${mongoUriw}`);
        });
      });
      } else {
      mongoose
        .connect(`mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useCreateIndex: true,
        })
        .then((res, err) => {
          if (err) return reject(err);
          return resolve();
        })
        .catch(err => reject(err));
    }
  });
}


function close() {
  return mongoose.disconnect();
}

module.exports = {
  connect,
  close,
};
